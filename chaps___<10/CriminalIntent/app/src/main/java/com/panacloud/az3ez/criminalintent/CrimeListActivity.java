package com.panacloud.az3ez.criminalintent;

import android.support.v4.app.Fragment;

/**
 * Created by panacloud on 3/25/15.
 */
public class CrimeListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }
}
