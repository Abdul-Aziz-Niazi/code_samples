package com.panacloud.az3ez.guru;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by panacloud on 2/28/15.
 */
public class QuizActivity extends Activity {
    private boolean mIsCheater;
    private RadioButton mTrueButton;
    private RadioButton mFalseButton;
    private ImageButton mNextButton;
    private Button mCheatButton;
    private TextView mQuestionTextView;
    public TextView mScoreTxt;
    boolean answerIsTrue;
    Toolbar t;
    int Score=0;

    private TrueFalse[] mQuestionBank = new TrueFalse[] {
            new TrueFalse(R.string.question_oceans, true),
            new TrueFalse(R.string.question_mideast, false),
            new TrueFalse(R.string.question_africa, false),
            new TrueFalse(R.string.question_americas, true),
            new TrueFalse(R.string.question_asia, true),
    };
    private int mCurrentIndex = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);
        mScoreTxt = (TextView) findViewById(R.id.scr_txt);
        mScoreTxt.setText("Score "+Score);
        mTrueButton = (RadioButton)findViewById(R.id.btn_true);
        mFalseButton = (RadioButton)findViewById(R.id.btn_false);
        mQuestionTextView = (TextView)findViewById(R.id.q_text);
        t = (Toolbar) findViewById(R.id.app_bar2);
        t.setTitle("Quiz");
        t.setSubtitle("Geography");
        mQuestionTextView.setText(mQuestionBank[mCurrentIndex].getQuestion());

//        mTrueButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkAnswer(true);
//                updateQuestion();
//            }
//        });
//        mFalseButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkAnswer(false);
//                updateQuestion();
//            }
//        });

        mNextButton = (ImageButton)findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsCheater = false;
                if(mTrueButton.isChecked()) {
                    checkAnswer(true);
                    mTrueButton.setChecked(false);
                    updateQuestion();
                }
                else if(mFalseButton.isChecked()){
                    checkAnswer(false);
                    mFalseButton.setChecked(false);
                    updateQuestion();
                }

            } });
//        mCheatButton = (Button)findViewById(R.id.cheat_button);
//        mCheatButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(QuizActivity.this,CheatActivity.class);
//                boolean answerIsTrue = mQuestionBank[mCurrentIndex].isTrueQuestion();
//                i.putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE, answerIsTrue);
//                startActivityForResult(i, 0);
//
//            }
//        });
    }
    private void checkAnswer(boolean userPressedTrue) {
        if(mCurrentIndex<5) {
             answerIsTrue = mQuestionBank[mCurrentIndex].isTrueQuestion();
        }
        if (mIsCheater) {

        } else {
            if (userPressedTrue == answerIsTrue) {
                Score+=10;

            } else {
                //nothing happens
            }
            if(Score<0) {
                finish();
                Toast.makeText(this, "Unfortunately You Failed Score : 0", Toast.LENGTH_SHORT)
                        .show();
            }
            else {
                mScoreTxt.setText("Score " + Score);
            }
        }

    }
    private void updateQuestion() {
       // Toast.makeText(this,""+mCurrentIndex,Toast.LENGTH_SHORT).show();
        mCurrentIndex = (mCurrentIndex + 1);
        if(mCurrentIndex < 5) {
            int question = mQuestionBank[mCurrentIndex].getQuestion();
            mQuestionTextView.setText(question);
        }
        else{
            finish();
            Toast.makeText(this, "Score : "+Score, Toast.LENGTH_SHORT)
                    .show();
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return; }
        mIsCheater = data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }
}
