package com.panacloud.az3ez.guru;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by panacloud on 2/12/15.
 */
public class Login extends Fragment{
    View view;
    Button bLogin;
    MyListener myListener;
    EditText etUser, etPass;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        myListener = (MyListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login,container,false);
        etUser = (EditText) view.findViewById(R.id.editText);
        bLogin = (Button) view.findViewById(R.id.btn_login);

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etUser.getText().toString().equals(""))
                myListener.listener(1, etUser.getText().toString());
            }
        });
        return view;
    }

}
