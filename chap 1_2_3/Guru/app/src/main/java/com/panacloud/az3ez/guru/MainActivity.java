package com.panacloud.az3ez.guru;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;


public class MainActivity extends ActionBarActivity implements MyListener{
    FragmentManager fm;
    FragmentTransaction ft;
    private Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    Home h;

    CharSequence Titles[]={"Home","Tests","Courses"};
    int NumbOftabs =3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
//        toolbar.setLogo(R.drawable.ic_launcher);
//        toolbar.setTitle("Hello");
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.add(R.id.frg,new Login());
        ft.commit();
        adapter = new ViewPagerAdapter(fm,Titles,NumbOftabs);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });
        h = new Home();
        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

    }

    @Override
    public void listener(int n,String su) {
        ft = fm.beginTransaction();
        if(n==1) {
            ft.replace(R.id.frg, h);
            toolbar.setSubtitle(su);
        }
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
